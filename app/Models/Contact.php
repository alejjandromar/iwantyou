<?php namespace iwantyou\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

  protected $table = 'contacts';
  protected $guarded = [];
  protected $hidden = [];


  public function user1() {
      return $this->belongsTo('iwantyou\User','users_id1');
  }

  public function user2() {
      return $this->belongsTo('iwantyou\User','users_id2');
  }
  

}