<?php namespace Repositories;
use Abstracts\Repository as AbstractRepository;
class MessageRepository extends AbstractRepository implements MessageRepositoryInterface
{

  protected $modelClassName = 'Models\Message';

}