<?php namespace Repositories;
use Abstracts\Repository as AbstractRepository;
class ContactRepository extends AbstractRepository implements ContactRepositoryInterface
{

  protected $modelClassName = 'Models\Contact';


  public function allLessBlock($users_id1){
    $className = '\\'.$this->modelClassName;
    return $className::orderBy('contacts.id','DESC')
    ->join('users','users.id','=','contacts.users_id2')
    ->where('contacts.is_blocked',0)
    ->where('contacts.active',1)
    ->where('users.active',1)
    ->where('users_id1',$users_id1)
    ->select(['contacts.is_favorite','users.*']) /// filtrar mas imagenes de usuario
    ->get();
  }

}