<?php namespace Repositories;
use Abstracts\Repository as AbstractRepository;
use Helpers\Image;
use Hash;
class UserRepository extends AbstractRepository implements UserRepositoryInterface
{

  protected $modelClassName = 'iwantyou\User';


  public function store(array $attributes){ 

    try{

      $obj = new $this->modelClassName;
      $hImage = new Image('');

      $image = $attributes['photo']['base64'];
      $mime = $attributes['photo']['filetype'];
      $find = $attributes['find']['id'];
      $orientation = $attributes['orientation']['id'];
      $disposition = $attributes['disposition']['id'];

      $obj->image = $hImage->resizeBase64img($image,$mime,118,139);
      $obj->image_for_chat = $hImage->resizeBase64img($image,$mime,40,40);
      $obj->mime = $mime;

      $obj->name = $attributes['name'];
      $obj->last_name = $attributes['last_name'];
      $obj->automatic_gps = $attributes['automatic_gps'];
      $obj->email = $attributes['email'];
      $obj->password = $attributes['password'];
      $obj->find = $find;
      $obj->orientation = $orientation;
      $obj->disposition = $disposition;

      $obj->save();

      return $obj;

    }catch(Exception $e){
      return false;
    }

  }

}