<?php namespace Repositories;
use Abstracts\Repository as AbstractRepository;
class RequestRepository extends AbstractRepository implements RequestRepositoryInterface
{

  protected $modelClassName = 'Models\Request';

}