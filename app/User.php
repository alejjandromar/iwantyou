<?php

namespace iwantyou;

use Illuminate\Foundation\Auth\User as Authenticatable;
use iwantyou\Models\Contact;
use iwantyou\Models\Request2;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function is_contact($id){
      //  dd(Contact::where('users_id1', $this->id)->where('users_id2', $id)->get());
        $this->iscontacto= count(Contact::where('users_id1', $this->id)->where('users_id2', $id)->get()) > 0? true:false;
        return $this;
        
    }
    
    public function is_request($id){
        $this->solicitado  = count(Request2::where('users_id1', $this->id)->where('users_id2', $id)->get()) > 0? true:false;
        $this->solicitante = count(Request2::where('users_id2', $this->id)->where('users_id1', $id)->get()) > 0? true:false;
    }
    
    public function contacts() {
        return $this->belongsToMany('iwantyou\User', 'contacts', 'users_id1', 'users_id2')->withPivot('is_favorite','is_blocked');
    }
    
}
