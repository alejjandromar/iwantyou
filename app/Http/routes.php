<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
  return view('welcome');
});


Route::any('users', 'UserController@store');
Route::any('users/{user_id}', 'UserController@store');

Route::post('sessions', 'SessionController@store');
//Route::any('sessions', 'SessionController@destroy');


Route::get('all_contacts_of/{users_id1}', 'ContactController@index');
Route::get('cerrar/{users_id1}', 'SessionController@destroy');
Route::get('no_ads/{users_id1}', 'ContactController@no_ads');
Route::get('around', 'AroundController@around_me');
Route::get('around_new', 'AroundController@around_me_new');
Route::get('agregarContacto', 'AroundController@i_want_you');
Route::get('agregarContacto_new', 'AroundController@i_want_you_new');
Route::get('obtener_solicitudes', 'AroundController@you_want_me');
Route::get('aceptar_solicit', 'AroundController@i_want_you_too');
Route::get('no_aceptar_solicit', 'AroundController@i_dont_want_you');
Route::get('mensajes', 'MessageController@get_messages');
Route::get('mensajes_all', 'MessageController@get_all_messages');
Route::get('n_mensajes', 'MessageController@get_n_messages');
Route::get('enviar_mensaje', 'MessageController@send_message');

Route::get('agregar_favoritos', 'AroundController@i_want_you_favorite');
Route::get('bloquear', 'AroundController@i_bloked_you');
Route::get('debug', 'AroundController@around_me_debug');