<?php

namespace iwantyou\Http\Controllers;

use Illuminate\Http\Request;
use iwantyou\Models\Contact;
use iwantyou\Models\Request2;
use iwantyou\User;
use Cache;
use Session;
use Auth;

class AroundController extends Controller {

    public function around_me_debug() {
        //0.0005
       /* $cont = 0;
        for ($index = 0; $index < 0.00045; $index+=0.000001) {
            for ($index1 = 0; $index1 < 0.00045; $index1+=0.000001) {
                $v = AroundController::getDistance(0, 0, $index, $index1, 'm') . ' m <br> ';

                if ($v == 50) {
                    $cont++;
                }
            }
        }
        echo $cont;*/
        $usuarios= Cache::get('usuarios');
        $ards = Cache::get('ards');
        $entro = Cache::get('entro');
        Cache::put('entro', "no", 1500);
		Cache::flush();
        return response()->json(['msg' => 'Success','ent' => $entro,/*'list'=>   Cache::get('us'),*/ 'usuarios' =>  Cache::get('usuarios'), 'ards' => Cache::get('ards')]);
    }

    public function around_me(Request $request) {
        $ards = json_decode($request->ards);
        $usuarios = Cache::get('usuarios', []);
        $users=[];
        //gps Search
        if ($request->has('lat') && $request->has('long')) {
            $usuarios[$request->user]["id"] =  $request->user;
            $usuarios[$request->user]["lat"] = $request->lat;
            $usuarios[$request->user]['long'] = $request->long;
            Cache::put('usuarios', $usuarios, 1500);
             Cache::put('entro', "si2", 1500);
            $user_log = User::find($request->user);
            foreach ($usuarios as $key => $user) {
               
                if ($user['id'] != ($request->user)) {
                    if ((abs($request->long - $user['long']) <= 0.00045)) {
                        if ((abs($request->lat - $user['lat']) <= 0.00045)) {
                            $us = User::find($user['id']);
                            if ($us->disposition == 1) {
                                if ($us->find == 1 || ($us->find == 2 && $us->find == $user_log->find && $us->orientation == $user_log->orientation)) {
                                    $us->is_contact($request->user);
                                    $us->is_request($request->user);
                                    $us->gps=true;
                                    if (!$us->iscontacto && !$us->solicitado && !$us->solicitante)
                                        $users[$user['id']] = $us;
                                }
                            }
                        }
                    }
                }
            }
             Cache::put('us', $users, 1500);
        }
        $cont = 0;
        //Bluetooth Search
        if (count($ards) > 0) {
            if ($user_log->disposition == 1) {
                $cont++;
                foreach ($ards as $key => $user_r) {
                    $cont++;
                    if (Cache::has($user_r->name)) {
                        $cont++;
                        $us = Cache::get($user_r->name);
                        if ($us->disposition == 1) {
                            if ($us->find == 1 || ($us->find == 2 && $us->find == $user_log->find && $us->orientation == $user_log->orientation)) {
                                $us->is_contact($request->user);
                                $us->is_request($request->user);
                                $us->gps=false;
                                if (!$us->iscontacto && !$us->solicitado && !$us->solicitante)
                                    $users[$us->id] = $us;
                            }
                        }
                    }
                }
            }
            Cache::put('ards', $ards, 1500);
            Cache::put('us', $users, 1500);
            Cache::put('c', $cont, 1500);
        }
        return response()->json(['msg' => 'Success', 'list' => $users, 'c' => Cache::get('c'), 'ards' => Cache::get('ards')]);
    }

    public function around_me_new(Request $request) {
        $device = json_decode($request->ard);
        Cache::put('device', $device, 1500);
        if (isset($device)) {
            if (Cache::has($device->name)) {
                $us = Cache::get($device->name);
                $us->is_contact($request->user);
                $us->is_request($request->user);
            }
        }
        return response()->json(['msg' => 'Success', 'new_us' => $us]);
    }

//Indica a otro usuario que quiere contactar
    public function i_want_you(Request $request) {
        $user1 = $request->user1;
        $user2 = $request->user2;
        $r = Request2::firstOrNew(['users_id1' => $user2, 'users_id2' => $user1]);
        $r->active = 1;
        $r->view = 1;
        $r->save();
        Cache::put($user2 . "not", true, 200);
        return response()->json(['msg' => 'Success']);
    }

//Obtiene la lista de contactos que quieren contactar contigo
    public function you_want_me_new(Request $request) {
        $user = $request->user1;
        $wants = Cache::pull("IWY_" + $user . "not");
        if ($wants) {
            $list = Request2::where('users_id1', $user)->where('active', 1)->where('view', 1)->get();
        }
        return response()->json(['msg' => 'Success', 'list' => $list], 200);
    }

//Obtiene la lista de contactos que quieren contactar contigo
    public function you_want_me(Request $request) {
        $user = $request->user1;
        $list = [];
        $listn = Request2::with('user2')->where('users_id1', $user)->where('active', 1)->get();
        foreach ($listn as $key => $value) {
            $value->user2 = $value->user2->is_contact($request->user1);

            $list[] = $value;
        }
        $list2 = Request2::with('user1')->where('users_id2', $user)->where('active', 1)->get();
        foreach ($list2 as $key => $value) {
            $value->m = true;
            $value->user1->m = true;
            $value->user1 = $value->user1->is_contact($request->user1);
            $list[] = $value;
        }
        return response()->json(['msg' => 'Success', 'list' => $list], 200);
    }

//Acepta a una persona
    public function i_want_you_too(Request $request) {
        $user = $request->user1;
        $user_contact = $request->user2;
//save contacts
        $r = Request2::firstOrNew(['users_id1' => $user, 'users_id2' => $user_contact]);
        $r->active = 3;
        $r->view = 1;
        $r->save();
        $r = Request2::firstOrNew(['users_id2' => $user, 'users_id1' => $user_contact]);
        $r->active = 3;
        $r->view = 1;
        $r->save();
        $contact = Contact::firstOrCreate(['users_id1' => $user, 'users_id2' => $user_contact]);
        $contact = Contact::firstOrCreate(['users_id2' => $user, 'users_id1' => $user_contact]);
//Guardar contacto
        return response()->json(['msg' => 'Success'], 200);
    }

//Acepta a una persona
    public function i_want_you_favorite(Request $request) {
        $user1 = $request->user1;
        $user2 = $request->user2;
//save contacts
        $contact = Contact::firstOrNew(['users_id1' => $user1, 'users_id2' => $user2]);
        $contact->is_favorite = $contact->is_favorite == 1 ? 0 : 1;
        $contact->save();
//Guardar contacto
        return response()->json(['msg' => 'Success'], 200);
    }

//Acepta a una persona
    public function i_bloked_you(Request $request) {
        $user1 = $request->user1;
        $user2 = $request->user2;
//save contacts
        $contact = Contact::firstOrNew(['users_id1' => $user1, 'users_id2' => $user2]);
        $contact->is_blocked = $contact->is_blocked == 1 ? 0 : 1;
        $contact->save();
//Guardar contacto
        return response()->json(['msg' => 'Success'], 200);
    }

//Acepta a una persona
    public function i_dont_want_you(Request $request) {
        $user = $request->user1;
        $user_contact = $request->user2;
//save contacts
        $r = Request2::firstOrNew(['users_id1' => $user, 'users_id2' => $user_contact]);
        $r->active = 4;
        $r->view = 0;
        $r->save();
        $r = Request2::firstOrNew(['users_id2' => $user, 'users_id1' => $user_contact]);
        $r->active = 3;
        $r->view = 1;
        $r->save();
//Guardar contacto
        return response()->json(['msg' => 'Success'], 200);
    }

    public static function getDistance($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km') {
        $theta = $longitude1 - $longitude2;
        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) +
                (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;
        switch ($unit) {
            case 'Mi':
                break;
            case 'Km' :
                $distance = $distance * 1.609344;
                break;
            case 'm' :
                $distance = $distance * 1.609344 * 1000;
                break;
        }
        return $distance;
    }

}
