<?php

namespace iwantyou\Http\Controllers;

use Illuminate\Http\Request;
use Abstracts\Repository as AbstractRepository;
//use Helpers\Image;
use iwantyou\Http\Requests;
use iwantyou\User;
use Cache;

//use Repositories\UserRepository as Repo;

class UserController extends Controller {

    //private $repo;

    function __construct(Request $request) {
        // $this->repo = $repo;
        $this->request = $request;
    }
    
    

    public function index() {
        //  $list = $this->repo->all();
        return response()->json(['msg' => 'Success', 'list' => $list], 200);
    }

    public function show($id) {
        // $obj =  $this->repo->find($id);
        return response()->json(['msg' => 'Success', 'obj' => $obj], 200);
    }

    public function store($user_id=null) {
        ini_set('memory_limit', '-1');
        $attributes = $this->request->all();
        try {
            if (!isset($user_id)){
                $obj = new User();
                $obj->password = bcrypt($attributes['password']);
                  $obj->email = $attributes['email'];
            }else{
                $obj = User::find($user_id);
            }
            $hImage = new Image('');
            $image = $attributes['photo']['base64'];
            $mime = $attributes['photo']['filetype'];
            $find = $attributes['find']['id'];
            $orientation = $attributes['orientation']['id'];
            $disposition = $attributes['disposition']['id'];
            $obj->image = $hImage->resizeBase64img($image, $mime, 118, 139);
            $obj->image_for_chat = $hImage->resizeBase64img($image, $mime, 40, 40);
            $obj->mime = $mime;

            $obj->name = $attributes['name'];
            $obj->last_name = $attributes['last_name'];
            $obj->automatic_gps = $attributes['automatic_gps'];
          
            $obj->find = $find;
            $obj->orientation = $orientation;
            $obj->disposition = $disposition;

            $obj->save();

        } catch (Exception $e) {
            return response()->json(['msg' => $e->getMessage()], 200);
        }
        return response()->json(['msg' => 'Success', 'user' => $obj], 200);
    }

}

class Image {

    private $image;

    function __construct($image) {
        $this->image = $image;
    }

    // obj = new Image(\Input::file('image'));


    public function getMime() {

        $imgpost = $this->image;
        return $imgpost->getClientOriginalExtension();
    }

    //obj->getMime;



    public function getBase64() {

        $imgpost = $this->image;

        $flujo = fopen($imgpost->getRealPath(), 'r');
        $enbase64 = base64_encode(fread($flujo, filesize($imgpost->getRealPath())));
        fclose($flujo);
        return $enbase64;
    }

    public function qualityBase64img($base64img, $mimeimg, $quality) {

        ob_start();
        $im = imagecreatefromstring(base64_decode($base64img));

        switch ($mimeimg) {
            case 'png':
            case 'image/png':
                imagepng($im, null, $quality);
                break;
            case 'jpg':
            case 'image/jpg':
            case 'jpeg':
            case 'image/jpeg':
                imagejpeg($im, null, $quality);
                break;
            case 'image/gif':
            case 'gif':
                imagegif($im, null, $quality);
        }


        $stream = ob_get_clean();
        $newB64 = base64_encode($stream);
        imagedestroy($im);
        return $newB64;
    }

    public function resizeBase64img($base64img, $mimeimg, $newwidth, $newheight) {
         try {
            // Get new sizes
            list($width, $height) = getimagesizefromstring(base64_decode($base64img));

            ob_start();
            $temp_thumb = imagecreatetruecolor($newwidth, $newheight);
            imagealphablending($temp_thumb, false);
            imagesavealpha($temp_thumb, true);

            $source = imagecreatefromstring(base64_decode($base64img));

            // Resize
            imagecopyresized($temp_thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);


            switch ($mimeimg) {
                case 'png':
                case 'image/png':
                case 'PNG':
                case 'IMAGE/PNG':
                    imagepng($temp_thumb, null);
                    break;
                case 'jpg':
                case 'image/jpg':
                case 'jpeg':
                case 'JPEG':
                case 'JPG':
                case 'IMAGE/JPG':
                case 'IMAGE/JPEG':
                case 'image/jpeg':
                    imagejpeg($temp_thumb, null);
                    break;
                case 'image/gif':
                case 'gif':
                case 'GIT':
                case 'IMAGE/GIF':
                    imagegif($temp_thumb, null);
            }

            $stream = ob_get_clean();
            $newB64 = base64_encode($stream);
            imagedestroy($temp_thumb);
            imagedestroy($source);
            return $newB64;
        }catch(FatalErrorException $e){
            return response()->json(['msg' => $e->getMessage()], 200);
        }
    }

    /*
     *
     * Los parametros son string base64, string mime, int alto deseado 
     */

    public function resizeBase64andScaleWidth($base64img, $mimeimg, $newheight) {

        // Get new sizes
        list($width, $height) = getimagesizefromstring(base64_decode($base64img));


        // Calcular nuevo ancho con la misma perdida o ganancia proporcial del alto
        $porNewHeight = ($newheight * 100) / $height;
        $newwidth = (int) ($width * ($porNewHeight / 100));

        ob_start();
        $temp_thumb = imagecreatetruecolor($newwidth, $newheight);
        imagealphablending($temp_thumb, false);
        imagesavealpha($temp_thumb, true);

        $source = imagecreatefromstring(base64_decode($base64img));

        // Resize
        imagecopyresized($temp_thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);


        switch ($mimeimg) {
            case 'png':
            case 'image/png':
            case 'PNG':
            case 'IMAGE/PNG':
                imagepng($temp_thumb, null);
                break;
            case 'jpg':
            case 'image/jpg':
            case 'jpeg':
            case 'JPEG':
            case 'JPG':
            case 'IMAGE/JPG':
            case 'IMAGE/JPEG':
            case 'image/jpeg':
                imagejpeg($temp_thumb, null);
                break;
            case 'image/gif':
            case 'gif':
            case 'GIT':
            case 'IMAGE/GIF':
                imagegif($temp_thumb, null);
        }

        $stream = ob_get_clean();
        $newB64 = base64_encode($stream);

        imagedestroy($temp_thumb);
        imagedestroy($source);

        return $newB64;
    }

}

