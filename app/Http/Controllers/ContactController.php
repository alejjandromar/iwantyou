<?php

namespace iwantyou\Http\Controllers;

use Illuminate\Http\Request;

use iwantyou\Http\Requests;

use iwantyou\User;

class ContactController extends Controller
{

  public function index($users_id1){ 
    $user = User::where('id',$users_id1)->with('contacts')->get();
    
    return response()->json(['msg' => 'Success','list' => $user[0]->contacts], 200);
  }
  
   public function no_ads($users_id1){ 
    $user = User::where('id',$users_id1)->update(['without_advertising' => 1]);
    return response()->json(['msg' => 'Success','list' => $user[0]], 200);
  }
  

  
  
}
