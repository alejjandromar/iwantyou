<?php

namespace iwantyou\Http\Controllers;

use Illuminate\Http\Request;
use iwantyou\Http\Requests;
use Auth;
use Cache;
use Session;

class SessionController extends Controller {

    function __construct(Request $request) {
        $this->request = $request;
    }

//  public function store(){
//    $inputs = $this->request->only(['email','password']);
//    $ru = new Repouser;
//    $alluser = $ru->all();
//    foreach ($alluser as $key => $user) {
//      if($inputs['password'] == $user->password && $inputs['email'] == $user->email){
//        Cache::put("IWY_"+$user->id,$user,200);
//        Session::put("user",$user,200);
//        return response()->json(['msg' => 'Success','res' => true,'user'=>$user], 200);
//      }
//    }
//
//    return response()->json(['msg' => 'Success','res' => false,'user'=>" "], 200);
//
//  }

    public function store(Request $request) {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $user->image=  $user->image_for_chat;
            Cache::put("IWY_" . $user->id, $user, 200);
            Session::put("user", $user, 200);
            return response()->json(['msg' => 'Success', 'res' => false, 'user' => $user], 200);
        } else {
            return response()->json(['msg' => 'Failed', 'res' => true, 'user' => " "], 200);
        }
    }

    public function destroy($id) {
       // dd(Auth::user());
        $usuarios = Cache::get('usuarios', []);
        if (isset($usuarios[$id])){
            unset($usuarios[$id]);
            Cache::put('usuarios', $usuarios, 1500);
        }
        Cache::forget("IWY_" . $id);
        
        Auth::logout();
    }

}
