<?php

namespace iwantyou\Http\Controllers;

use Illuminate\Http\Request;

use iwantyou\Http\Requests;
use iwantyou\Models\Message;
use iwantyou\User;
use Carbon\Carbon;

class MessageController extends Controller
{
  //Obtiene msjs
  public function get_messages(Request $request){ 
    $user=$request->user1;
    $user2=$request->user2;
    $msj=  \iwantyou\Models\Message::whereRaw('(users_id1= ? and users_id2=?) or (users_id1= ? and users_id2=?)',[$user,$user2,$user2,$user])->with('user1','user2')->get();
    \iwantyou\Models\Message::whereRaw('(users_id1= ? and users_id2=?) or (users_id1= ? and users_id2=?)',[$user,$user2,$user2,$user])->with('user1','user2')->update(['viewed' => 1]);
    if (isset($msj[0])){
        return response()->json(['msg' => 'Success','list' => $msj], 200);
    }else{
        return response()->json(['msg' => 'No sms'], 200);
    }
  }
  
  public function get_n_messages(Request $request){ 
    $user=$request->user1;
    $user2=$request->user2;
    $fecha= $request->fecha;
    $msj=  \iwantyou\Models\Message::whereRaw('((users_id1= ? and users_id2=?) or (users_id1= ? and users_id2=?)) and created_at > ?',[$user,$user2,$user2,$user,$fecha])->get();
     \iwantyou\Models\Message::whereRaw('(users_id1= ? and users_id2=?) or (users_id1= ? and users_id2=?) and created_at > ?',[$user,$user2,$user2,$user,$fecha])->update(['viewed' => 1]);
    if (isset($msj[0])){
        return response()->json(['msg' => 'Success','list' => $msj], 200);
    }else{
        return response()->json(['msg' => 'No sms'], 200);
    }
  }
  
  public function get_old_messages(Request $request){ 
    $user=$request->user1;
    $user2=$request->user2;
    $fecha= $request->fecha;
    $msj=  \iwantyou\Models\Message::whereRaw('((users_id1= ? and users_id2=?) or (users_id1= ? and users_id2=?)) and ? < created_at',[$user,$user2,$user2,$user,$fecha])->get();
     if (isset($msj[0])){
        return response()->json(['msg' => 'Success','list' => $msj], 200);
    }else{
        return response()->json(['msg' => 'No sms'], 200);
    }
  }
  
  public function get_all_messages(Request $request){ 
    $user=$request->user1;
    $user_log = User::find($request->user1);
    $fecha= $user_log->last_m;
 
    $msj=  \iwantyou\Models\Message::whereRaw('(users_id1= ? or users_id2= ?) and created_at > ?  ',[$user,$user,$fecha])->get();
       $user_log->last_m=Carbon::now();
    $user_log->save();
    if (isset($msj[0])){
        return response()->json(['msg' => 'Success','list' => $msj, 'list2' => true], 200);
    }else{
        return response()->json(['msg' => 'No sms'], 200);
    }
  }
  
  //Obtiene msjs
  public function send_message(Request $request){ 
     $contact = \iwantyou\Models\Contact::where('users_id1' ,$request->user1)->where('users_id2'  ,$request->user2)->get()[0];
     $contact2 = \iwantyou\Models\Contact::where('users_id1' ,$request->user2)->where('users_id2'  ,$request->user1)->get()[0];
     if ($contact->is_blocked == 0 && $contact2->is_blocked == 0){
        \iwantyou\Models\Message::create(['users_id1' => $request->user1,'users_id2'  => $request->user2,'content' => $request->contenido]);
        return response()->json(['msg' => 'Success'], 200);
     }else{
          return response()->json(['msg' => 'bloqueado'], 200);
     }
  }
  
}
